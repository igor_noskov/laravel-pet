@extends('admin.index')

@section('title')
Добавление пользователя
@stop

@section('styles')

@stop

@section('row')
<div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">User create</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @if($errors->any())
                        <ul style="list-style-type: none;" class = "alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <!-- form start -->
                {!! Form::open(['url' => '/admin/users']) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Введите имя администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('login', 'Login') !!}
                            {!! Form::text('login', null, ['class' => 'form-control', 'placeholder' => 'Введите логин администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Введите e-mail администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Password') !!}
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Введите пароль администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Password again') !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Введите еще раз пароль администратора']) !!}
                        </div>

                        <div class="box-footer">
                            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
              </div><!-- /.box -->
              </div></div>
@stop

@section('scripts')

@stop