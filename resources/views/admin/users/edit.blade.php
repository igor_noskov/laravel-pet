@extends('admin.index')

@section('title')
Редактирование пользователя
@stop

@section('styles')

@stop

@section('row')
<div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Редактирование пользователя</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @if($errors->any())
                        <ul style="list-style-type: none;" class = "alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <!-- form start -->
                {!! Form::model($user, ['method' => 'PATCH', 'action' => ['admin\UsersController@update', $user->id]]) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Имя') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Введите имя администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('login', 'Логин') !!}
                            {!! Form::text('login', null, ['class' => 'form-control', 'placeholder' => 'Введите логин администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Введите e-mail администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Пароль') !!}
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Введите пароль администратора']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Пароль еще раз') !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Введите еще раз пароль администратора']) !!}
                        </div>

                        <div class="box-footer">
                            {!! Form::submit('Обновить', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
              </div><!-- /.box -->
@stop

@section('scripts')

@stop