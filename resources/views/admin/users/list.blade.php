@extends('admin.index')

@section('title')
Пользователи
@stop

@section('styles')
    {!! Html::style('js/admin/plugins/datatables/dataTables.bootstrap.css') !!}
    {!! Html::style('js/admin/plugins/jquery-confirm/jquery-confirm.min.css') !!}
@stop

@section('row')
    <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Users</h3>
                      <a style="float: right;" href="/admin/users/create" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> Create</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="users" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Login</th>
                            <th>E-mail</th>
                            <th>Created</th>
                            <th>Edited</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                          <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->login }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ Carbon::parse($user->created_at)->format("Y-m-d") }}</td>
                            <td>{{ Carbon::parse($user->updated_at)->format("Y-m-d") }}</td>
                            <td>
                                <a style="margin-left: 5px;" href="{{action('admin\UsersController@edit', $user->id)}}" title="Редактировать"><span class="glyphicon glyphicon-edit"></span></a>
                                <a style="margin-left: 5px; cursor: pointer;" class = "glyphicon glyphicon-trash delete" href="{{url('admin/users', $user->id)}}" data-method="delete" data-confirm="Are you sure?"></a>
                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
@stop

@section('scripts')
    {!! Html::script('js/admin/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('js/admin/plugins/datatables/dataTables.bootstrap.min.js') !!}
    {!! Html::script('js/admin/plugins/jquery-confirm/jquery-confirm.min.js') !!}
    <script type="text/javascript">
      $(function () {
        $('#users').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });

    $(function(){
        $('[data-method]').append(function(){
            return "\n"+
            "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>\n"+
            "   <input type='hidden' name='_method' value='"+$(this).attr('data-method')+"'>\n"+
            "   <input type='hidden' name='_token' value='<?php echo csrf_token(); ?>'>\n"+
            "</form>\n"
        })
            .removeAttr('href');
    });

    $('.glyphicon-trash').on('click', function () {
        var button = $(this);

        $.confirm({
            title: 'Предупреждение удаления',
            content: 'Вы уверены, что хотите удалить данного администратора?',
            confirm: function () {
                $(button).children('form').submit();
            }
        });
    });
    </script>
@stop