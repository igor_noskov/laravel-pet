@section('css')
    {!! Html::style('css/admin/style.css') !!}
    {!! Html::style('css/admin/bootstrap.min.css') !!}
    {!! Html::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
    {!! Html::style('css/admin/ionicons.min.css') !!}
    {!! Html::style('css/admin/AdminLTE.min.css') !!}
    {!! Html::style('css/admin/plugins/datepicker3.css') !!}
    {!! Html::style('css/admin/skins/_all-skins.min.css') !!}
@stop

@section('js')
    {!! Html::script('js/admin/plugins/jQuery/jQuery-2.1.4.min.js') !!}
    {!! Html::script('js/admin/plugins/jQueryUI/jquery-ui.min.js') !!}
    {!! Html::script('js/admin/bootstrap.min.js') !!}
    {!! Html::script('js/admin/moment.min.js') !!}
    {!! Html::script('js/admin/plugins/daterangepicker/daterangepicker.js') !!}
    {!! Html::script('js/admin/plugins/datepicker/bootstrap-datepicker.js') !!}
    {!! Html::script('js/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
    {!! Html::script('js/admin/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('js/admin/plugins/fastclick/fastclick.min.js') !!}
    {!! Html::script('js/admin/app.min.js') !!}
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
@stop

@section('if_ie')
    {!! Html::script('js/admin/htl5shiv.min.js') !!}
    {!! Html::script('js/admin/respond.min.js') !!}
@stop