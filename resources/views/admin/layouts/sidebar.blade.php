<ul class="sidebar-menu">
            <li class="header">ADMIN PANEL</li>
            <li class = "@if (Request::is('admin/users')) active @endif">
              <a href="/admin/users/">
                <i class="fa fa-users"></i> <span>Users</span>
              </a>
            </li>
            <li class = "@if (Request::is('admin/portfolio')) active @endif">
                <a href="/admin/subscribers">
                  <i class="fa fa-server"></i> <span>Portfolio</span>
                </a>
            </li>
          </ul>